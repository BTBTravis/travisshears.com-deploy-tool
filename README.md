# Deploy tool for deploying travisshears.com

This is a little cli tool I built because I was not satisfied with aws-cli's sync command.

It works with aws's boto3 python lib and the click framework.

The tool is hyper spefic to my use case so it includes hard coded bucket ids and such...

[![asciicast](https://asciinema.org/a/341429.svg)](https://asciinema.org/a/341429)

## Install

The python app is available on my custom brew tap.

```shell
$ brew tap btbtravis/tap
$ brew install deploy_tool
```

## Dev

To use on local it is quite easy to setup. Simply clone the project and run the following commands:

```shell
$ pipenv install
$ pipenv shell
$ pip install --editable .
$ deploy_tool --version
```
