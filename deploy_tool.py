import click

from tool import DeployTool

@click.version_option()

@click.group()
def cli():
    pass

@cli.command()
@click.option('--lang', required=True, help='language to deploy en or de')
def upload(lang):
    """Upload site to s3"""
    print(f"Deploying {lang}")
    deploy_tool = DeployTool(lang)
    deploy_tool.upload()


@cli.command()
@click.option('--lang', required=True, help='language to deploy en or de')
def invalidate(lang):
    """Invalidate cloudfront cache"""
    print(f"Invlidating {lang}")
    deploy_tool = DeployTool(lang)
    invalidation_id = deploy_tool.invalidate()
    print(f"Created invalidation with id: {invalidation_id}")

if __name__ == '__main__':
    cli()
