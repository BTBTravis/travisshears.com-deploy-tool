from setuptools import setup, find_packages

setup(
    name='travisshears-deploy-tool',
    version='0.1.1',
    packages=find_packages(),
    include_package_data=True,
    python_requires='~=3.7',
    py_modules=['deploy_tool'],
    install_requires=[
        'click>=7.1.2',
        'boto3>=1.12.49'
    ],
    entry_points={
        'console_scripts': ['deploy_tool = deploy_tool:cli']
    }
)
