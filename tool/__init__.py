import subprocess
from subprocess import PIPE, Popen
import sys
import pprint
import boto3
import time
import json
import re
import mimetypes

class DeployTool:
    def __init__(self, lang):
        # Assign the argument to the instance's name attribute
        self._lang = lang
        # setup s3
        session = boto3.Session(profile_name='personal')
        self._s3 = session.resource('s3') 
        self._cloud_front = session.client('cloudfront')
        self._config = {
            'bucket': {
                'de': 'travisshears.de',
                'en': 'travisshears.com'
            },
            'cloud-front-distribution-id': {
                'de': 'E1HY5O9F1FWCA',
                'en': 'E29OAXKYAP0NP8'
            }
        }

    def upload(self):
        """upload site to s3"""
        # get local file hashes
        raw_hashes = self._get_local_raw_hashes()
        local_hashes_by_filename = self._hashes_by_filename(raw_hashes)

        # get remote file hashes
        remote_hashs = self._s3.Object(self._config['bucket'][self._lang], 'build_hashes.json')
        body = remote_hashs.get()['Body'].read()
        remote_hashs_by_filename = json.loads(body.decode("utf-8"))

        # check which files to upload
        paths = self._paths_to_upload(local_hashes_by_filename, remote_hashs_by_filename)
        if len(paths) == 0:
            print("no files to upload")

        # upload files to s3
        for path in paths:
            print(f"uploading {path}")
            clean_path = re.sub(r"\./public\/[a-z][a-z]\/", "", path)
            mime_type = mimetypes.guess_type(clean_path)[0]
            if mime_type is None:
                mime_type = 'application/octet-stream'
            self._s3.Object(self._config['bucket'][self._lang], clean_path).put(ACL='public-read', ContentType=mime_type, Body=open(path, 'rb'))

        # upload local hashes to remote
        hashes_json = json.dumps(local_hashes_by_filename)
        hashes_json_bytes = str.encode(hashes_json)
        remote_hashs.put(Body=hashes_json_bytes)

    # def _get_remote_hashes(self):

    def invalidate(self):
        """invlidate cloudfront cache for site"""
        invalidation_obj = {
            'Paths': {
                'Items': ['/*'],
                'Quantity': 1,
            },
            'CallerReference': 'deploy-tool-{}'.format(str(time.time()))
        }

        r = self._cloud_front.create_invalidation(
            DistributionId=self._config['cloud-front-distribution-id'][self._lang],
            InvalidationBatch=invalidation_obj
        )
        return r['Invalidation']['Id']

    def _paths_to_upload(self, new_hashes, old_hashes):
        paths_to_upload = []
        for new_path,new_hash in new_hashes.items():
            if new_path in old_hashes and new_hash == old_hashes[new_path]:
                continue
            paths_to_upload.append(new_path)
        return paths_to_upload

    def _get_local_raw_hashes(self):
        """Generate md5 checksum hashes via the command line"""
        md5_command = 'md5' if sys.platform.startswith('darwin') else 'md4sum'
        find_process = Popen(['find', f'./public/{self._lang}', '-type', 'f'], stdout=PIPE)
        hash_process = Popen(['xargs', md5_command, '-r'], stdin=find_process.stdout, stdout=PIPE)
        find_process.stdout.close()
        stdout,stderr = hash_process.communicate()
        return stdout

    @staticmethod
    def _hashes_by_filename(raw_hashes):
        """Process a raw input of hashes and filenames and make a dict"""
        lines = raw_hashes.splitlines()
        hashes_by_filename = {}
        for line in lines:
            decoded_line = line.decode("utf-8") # "fe14d2 ./public/index.html"
            parts = decoded_line.split(' ') # ["fe14d2", "./public/index.html"]
            hashes_by_filename[parts[1]] = parts[0]
        return hashes_by_filename
