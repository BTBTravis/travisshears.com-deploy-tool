from unittest import TestCase, mock
import tool

class TestTool(TestCase):
    def test_hashes_by_filename(self):
        mock_raw_data = b'sasfjafe1231 ./public/index.html\nfsieh8sfhe8hf8esh ./public/img.gif\nsf8ahf8hea8fehf8s ./public/test.txt'
        t = tool.DeployTool('en')
        hashes_by_filename = t._hashes_by_filename(mock_raw_data)
        self.assertEqual(hashes_by_filename, {
            './public/index.html': 'sasfjafe1231',
            './public/img.gif': 'fsieh8sfhe8hf8esh',
            './public/test.txt': 'sf8ahf8hea8fehf8s'
        })
    
    def test_paths_to_upload(self):
        mock_new_hashes = {
            './a/file1': 'abcd',
            './a/file2': 'cccc',
            './a/file3': 'bbbb',
            './a/file4': 'abcd',
        }
        mock_old_hashes = {
            './a/file1': 'abcd',
            './a/file2': 'abcd',
            './a/file3': 'abcd',
            './a/file4': 'abcd',
        }

        t = tool.DeployTool('en')
        paths = t._paths_to_upload(mock_new_hashes, mock_old_hashes) 
        self.assertEqual(paths, [
            './a/file2',
            './a/file3'
        ])